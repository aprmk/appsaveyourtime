package com.example.awesomeapp

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.geeksforgeeks.myfirstkotlinapp.R

class SettingsActivityFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }
}